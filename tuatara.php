<?php

function output($input){
	$array = is_array($input);
	$object = is_object($input);
		if($array==TRUE){
			foreach($input as $line){
				echo $line.'<br>';
			}
			return;
		}
		elseif($object==$true){
			die('An object cannot be used with output()');
		}else{
			echo $input;
			return;
		}
}
function get($file){
	$contents = file_get_contents($file);
	return $contents;
}

function import($src){
	if(isset($src)){
		require($_SERVER['DOCUMENT_ROOT'].$src);
		return;
	}else{
		throw new Exception('Cannot open nothing');
	}
}

function slice($input, $slice) {
     $arg = explode(':', $slice);
     $start = intval($arg[0]);
     if ($start < 0) {
         $start += strlen($input);
     }
     if (count($arg) === 1) {
         return substr($input, $start, 1);
     }
     if (trim($arg[1]) === '') {
         return substr($input, $start);
     }
     $end = intval($arg[1]);
     if ($end < 0) {
         $end += strlen($input);
     }
     return substr($input, $start, $end - $start);
}
 

class object{
}
